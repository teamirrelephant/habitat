﻿using System.Collections.Generic;
using UnityEngine;
using Habitat.Game;
using System;
using Habitat.DynamicTerrain.Deformation;
using Habitat.DataStructures;

namespace Habitat.DynamicTerrain
{
	/// <summary>
	/// Compound terrain object.
	/// </summary>
	public class SuperTerrain 
	{
		/// <summary>
		/// Contains the array of subterrain objects
		/// </summary>
		private Terrain[,] subterrains;
		
		/// <summary>
		/// Superterrain detail. The resultsing superterrain is 2^detail terrains.
		/// </summary>
		/// <value>The detail.</value>
		public int Detail { get; private set; }
        
        ///<summary>
        ///Resolution of each terrain in the SuperTerrain;
        ///</summary>
        private readonly int hmResolution = GameplayConstants.SuperTerrainHeightmapResolution;
		
		/// <summary>
		/// Parent gameobject to nest created terrains into.
		/// </summary>
		/// <value>The parent.</value>
		public Transform Parent { get; private set; }
		
		/// <summary>
		/// Builds the new terrain object.
		/// </summary>
		/// <returns>The new terrain.</returns>
		private Terrain BuildNewTerrain()
		{
			//Using this divisor because of internal workings of the engine. The resulting terrain is still going to be subterrain size.
			var divisor = GameplayConstants.SuperTerrainHeightmapResolution / GameplayConstants.SubterrainSize * 2;

			var terrainData = new TerrainData {
				size = new Vector3 (GameplayConstants.SubterrainSize / divisor, GameplayConstants.SuperTerrainHeightHeight, GameplayConstants.SubterrainSize / divisor),
				heightmapResolution = GameplayConstants.SuperTerrainHeightmapResolution
			};
			
			var newTerrain = Terrain.CreateTerrainGameObject(terrainData).GetComponent<Terrain>();
			
			newTerrain.transform.parent = Parent;
			newTerrain.transform.gameObject.layer = GameplayConstants.TerrainLayer;
			newTerrain.heightmapPixelError = GameplayConstants.SuperTerrainPixelError;
			newTerrain.gameObject.AddComponent<Rigidbody>();
			var terrainBody = newTerrain.GetComponent<Rigidbody>();
			terrainBody.useGravity = false;
			terrainBody.isKinematic = true;
			newTerrain.gameObject.isStatic = false;
			
			return newTerrain;
		}
		
		/// <summary>
		/// Initializez the terrain array and moves the terrain transforms to match their position in the array.
		/// </summary>
		private void InitializeTerrainArray()
		{
			subterrains = new Terrain[Detail, Detail];
			
			for (int x = 0; x < Detail; x++) {
				for (int y = 0; y < Detail; y++) {
					subterrains[y, x] = BuildNewTerrain();
					subterrains[y, x].transform.Translate(new Vector3(x * GameplayConstants.SubterrainSize,
					                                                  0f,
					                                                  y * GameplayConstants.SubterrainSize));
				}
			}
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SuperTerrain"/> class.
		/// </summary>
		/// <param name="detail">Superterrain detail. The resultsing superterrain is 2^detail terrains.</param>
		/// <param name="parent">Parent gameobject to nest created terrains into.</param>
		public SuperTerrain(int detail, Transform parent)
		{
			Detail = detail;
			Parent = parent;
			
			InitializeTerrainArray();
			SetNeighbors();
		}

		#region [ Array Helpers ]
		/// <summary>
		/// Safelys retrieves the terrain obejct from the array.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		private Terrain SafeGetTerrain(int x, int y)
		{
			if (x < 0 || y < 0 || x >= Detail || y >= Detail)
				return null;
			return subterrains[y, x];
		}

		/// <summary>
		/// Iterates over terrain object and executes the given action
		/// </summary>
		/// <param name="lambda">Lambda.</param>
		private void ForEachSubterrain(Action<int, int, Terrain> lambda) {
			for (int x = 0; x < Detail; x++) {
				for (int y = 0; y < Detail; y++) {
					lambda (x, y, SafeGetTerrain(x, y));
				}
			}
		}

		#endregion

		/// <summary>
		/// Iterates through the terrain object and sets the neightbours to match LOD settings.
		/// </summary>
		private void SetNeighbors()
		{
			ForEachSubterrain ((x, y, subterrain) => {
				subterrain.SetNeighbors(
						SafeGetTerrain(x - 1, y),
						SafeGetTerrain(x, y + 1),
						SafeGetTerrain(x + 1, y),
						SafeGetTerrain(x, y - 1));
			});
		}

		/// <summary>
		/// Sets the global heightmap to match the given one. Give heightmap must match the (SubterrainHeightmapResolution * Detail).
		/// </summary>
		/// <param name="heightmap">Heightmap to set the heights from.</param>
		public void SetGlobalHeightmap(float[,] heightmap) {
			ForEachSubterrain((x, y, subterrain) => {
				var chunkStartX = x * GameplayConstants.SuperTerrainHeightmapResolution;
				var chunkStartY = y * GameplayConstants.SuperTerrainHeightmapResolution;
				subterrains[y, x].terrainData.SetHeights(0, 0,
				                    GetSubHeightMap(heightmap,
					                chunkStartX + GameplayConstants.SuperTerrainHeightmapResolution + 1,
					                chunkStartY + GameplayConstants.SuperTerrainHeightmapResolution + 1,
					                chunkStartX,
					                chunkStartY));
					
			});
		}
		
		/// <summary>
		/// Retrieves the minor heightmap from the entire heightmap array.
		/// </summary>
		/// <returns>The minor height map.</returns>
		/// <param name="heightMap">Major heightmap.</param>
		/// <param name="Xborder">Xborder.</param>
		/// <param name="Yborder">Yborder.</param>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		private float[,] GetSubHeightMap (float[,] heightMap, int Xborder, int Yborder, int x, int y)
		{
			if (Xborder == x || Yborder == y || x < 0 || y < 0)
				return null;

			var temp = new float[Yborder - y, Xborder - x];
			for (int i = x; i < Xborder; i++) {
				for(int j = y; j < Yborder; j++){
					temp[j - y, i - x] = heightMap[j, i];
				}
			}
			return temp;
		}

		/// <summary>
		/// Applies the partial heightmap to a single terrain object.
		/// </summary>
		/// <param name="heightmap">Heightmap.</param>
		/// <param name="chunkX">Terrain x.</param>
		/// <param name="chunkY">Terrain y.</param>
		/// <param name="startX">Start x.</param>
		/// <param name="startY">Start y.</param>
		/// <param name="type">Deformation type.</param>
		private void ApplyPartialHeightmap(float[,] heightmap, int chunkX, int chunkY, int startX, int startY, TerrainDeformation td)
		{
			if (heightmap == null)
				return;

			var current = subterrains [chunkY, chunkX].terrainData.GetHeights (startX, startY, heightmap.GetUpperBound (1) + 1, heightmap.GetUpperBound (0) + 1); 
			for (int x = 0; x <= heightmap.GetUpperBound(1); x++) {
				for (int y = 0; y <= heightmap.GetUpperBound(0); y++) {
					current[y, x] = td.ApplyToPoint(current[y, x], heightmap[y, x]);
				}
			}
			subterrains[chunkY, chunkX].terrainData.SetHeights (startX, startY, current);
		}

		private int TransformCoordinate (float coordinate)
		{
			return Mathf.RoundToInt(coordinate * GameplayConstants.SuperTerrainHeightmapResolution / GameplayConstants.SubterrainSize);
		}

		/// <summary>
		/// Applies the local deformation.
		/// </summary>
		/// <param name="deformation">Deformation.</param>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		public void ApplyDeformation(TerrainDeformation td, float xCoord, float yCoord) {
			int x = TransformCoordinate (xCoord);
			int y = TransformCoordinate (yCoord);

			var chunkX = x / hmResolution;
			var chunkY = y / hmResolution;

            ApplyPartialHeightmap(GetBottomLeftSubmap(td, x, y), chunkX - 1, chunkY - 1, hmResolution, hmResolution, td);
			ApplyPartialHeightmap(GetLeftSubmap(td, x, y), chunkX - 1, chunkY, hmResolution, y % hmResolution, td);
			ApplyPartialHeightmap(GetTopLeftSubmap(td, x, y), chunkX - 1, chunkY + 1, hmResolution, 0, td);
			ApplyPartialHeightmap(GetBottomSubmap(td, x, y), chunkX, chunkY - 1, x % hmResolution, hmResolution, td);
			ApplyPartialHeightmap(GetBottomRightSubmap(td, x, y), chunkX + 1, chunkY - 1, 0, hmResolution, td);
			ApplyPartialHeightmap(GetMiddleSubmap(td, x, y), chunkX, chunkY, x % hmResolution, y % hmResolution, td);
			ApplyPartialHeightmap(GetTopSubmap(td, x, y), chunkX, chunkY + 1, x % hmResolution, 0, td);
			ApplyPartialHeightmap(GetRightSubmap(td, x, y), chunkX + 1, chunkY, 0, y % hmResolution, td);   
			ApplyPartialHeightmap(GetTopRightSubmap(td, x, y), chunkX + 1, chunkY + 1, 0, 0, td);            
		}
        
        ///Retrieves the bottom-left part of the deformation (Subheightmap, applied to the bottom
        ///left chunk of the targetChunk) or null if no such submap has to be applied.
        ///Covers corner cases
        private float[,] GetBottomLeftSubmap(TerrainDeformation td, int x, int y) {
            if (x % hmResolution == 0 && y % hmResolution == 0 && x / hmResolution > 0 && y / hmResolution > 0)
            {
                return new float[,] {{ td.Heightmap[0, 0] }};
            }
            return null;
        }
        
        ///Retrieves the left part of the deformation (Subheightmap, applied to the
        ///left chunk of the targetChunk) or null if no such submap has to be applied.
        ///Covers edge cases        
        private float[,] GetLeftSubmap(TerrainDeformation td, int x, int y) {
            if (x % hmResolution == 0 && x / hmResolution > 0)
            {
                int endY = Math.Min((y / hmResolution + 1) * hmResolution, y + td.H);
                return GetSubHeightMap(td.Heightmap, 1, endY - y, 0, 0);
            }
            return null;
        }
        
        ///Retrieves the bottom part of the deformation (Subheightmap, applied to the bottom
        ///chunk of the targetChunk) or null if no such submap has to be applied.
        ///Covers edge cases
        private float[,] GetBottomSubmap(TerrainDeformation td, int x, int y) {
            if (y % hmResolution == 0 && y / hmResolution > 0)
            {
                int endX = Math.Min((x / hmResolution + 1) * hmResolution, x + td.W);
                return GetSubHeightMap(td.Heightmap, endX - x, 1, 0, 0);
            }
            return null;
        }
        
        ///Retrieves the top-left part of the deformation (Subheightmap, applied to the top
        ///left chunk of the targetChunk) or null if no such submap has to be applied.
        ///Covers split edge cases
        private float[,] GetTopLeftSubmap(TerrainDeformation td, int x, int y) {
            if (x % hmResolution == 0 && x / hmResolution > 0)
            {
                int startY = (y / hmResolution + 1) * hmResolution;
                int endY = y + td.H;
                if (startY > endY) return null;
                return GetSubHeightMap(td.Heightmap, 1, td.H, 0, startY - y);
            }
            return null;
        }

        ///Retrieves the bottom-right part of the deformation (Subheightmap, applied to the bottom
        ///right chunk of the targetChunk) or null if no such submap has to be applied.
        ///Covers split edge cases
        private float[,] GetBottomRightSubmap(TerrainDeformation td, int x, int y) {
            if (y % hmResolution == 0 && y / hmResolution > 0)
            {
                int startX = (x / hmResolution + 1) * hmResolution;
                int endX = x + td.W;
                if (startX > endX) return null;
                return GetSubHeightMap(td.Heightmap, td.W, 1, startX - x, 0);
            }
            return null;
        }
        
        ///Retrieves the main deformation part.
        private float[,] GetMiddleSubmap(TerrainDeformation td, int x, int y) {
            int endX = Math.Min((x / hmResolution + 1) * hmResolution, x + td.W);
            int endY = Math.Min((y / hmResolution + 1) * hmResolution, y + td.H);
            return GetSubHeightMap(td.Heightmap,
			                       Math.Min(endX - x + 1, td.Heightmap.GetUpperBound(0) + 1),
			                       Math.Min(endY - y + 1, td.Heightmap.GetUpperBound(1) + 1),
			                       0, 0);
        }
        ///Retrieves the top deformation part or null if none required
        private float[,] GetTopSubmap(TerrainDeformation td, int x, int y) {
            int startY = (y / hmResolution + 1) * hmResolution;
            if (y + td.H < startY) return null;
            int endX = Math.Min((x / hmResolution + 1) * hmResolution, x + td.W);  
            return GetSubHeightMap(td.Heightmap, Math.Min (endX - x + 1, td.Heightmap.GetUpperBound(0) + 1), td.H, 0, startY - y); 
        }
        
        ///Retrieves the left deformation part or null if none required
        private float[,] GetRightSubmap(TerrainDeformation td, int x, int y) {
            int startX = (x / hmResolution + 1) * hmResolution;
            if (x + td.W < startX) return null;
            int endY = Math.Min((y / hmResolution + 1) * hmResolution, y + td.H);
			return GetSubHeightMap(td.Heightmap, td.W, Math.Min(endY - y + 1, td.Heightmap.GetUpperBound(1) + 1), startX - x, 0);
        }
        
        ///Retrieves the top-right part of the main deformation.
        private float[,] GetTopRightSubmap(TerrainDeformation td, int x, int y) {
            int startX = (x / hmResolution + 1) * hmResolution;
            int startY = (y / hmResolution + 1) * hmResolution;
            if (x + td.W < startX || y + td.H < startY) return null;
            return GetSubHeightMap(td.Heightmap, td.W, td.H, startX - x, startY - y);
        }
	}
}
