﻿using System;
using UnityEngine;
using System.Collections;
using Habitat.Parallel;
using Habitat.DataStructures;

namespace Habitat.DynamicTerrain.Deformation {
	public abstract class TerrainDeformation
    {
		/// <summary>
		/// Height of the deformation in hightmap pixels.
		/// </summary>
        public int H { get; private set; }

		/// <summary>
		/// Width of the deformation in hightmap pixels.
		/// </summary>
        public int W { get; private set; }

		/// <summary>
		/// Heightmap matrix object
		/// </summary>
        public float[,] Heightmap { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Habitat.DynamicTerrain.Deformation.TerrainDeformation"/> class.
		/// </summary>
		/// <param name="height">Height in heightmap pixels</param>
		/// <param name="width">Width in heightmap pixels</param>
        protected TerrainDeformation(int height, int width)
        {
            H = height;
            W = width;
			Heightmap = new float[height,width];
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="Habitat.DynamicTerrain.Deformation.TerrainDeformation"/> class.
		/// </summary>
		/// <param name="bitmap">Normalized heightmap matrix.</param>
        protected TerrainDeformation(float[,] bitmap)
        {
            Heightmap = bitmap;
            H = bitmap.GetUpperBound(0);
            W = bitmap.GetUpperBound(1);
        }

		/// <summary>
		/// Applies deformation to the point. Additive by default.
		/// </summary>
		/// <returns>The to point.</returns>
		/// <param name="currentValue">Current value.</param>
		/// <param name="newValue">New value.</param>
		public virtual float ApplyToPoint(float currentValue, float newValue) {
			return currentValue + newValue;
		}

		/// <summary>
		/// Generates the heightmap matrix based on constructor parameters.
		/// </summary>
		public abstract TerrainDeformation Generate();
    }
}