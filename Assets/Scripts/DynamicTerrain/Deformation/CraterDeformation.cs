﻿using System;
using UnityEngine;
using Habitat.Game;

namespace Habitat.DynamicTerrain.Deformation
{
    public class CraterDeformation : TerrainDeformation
    {
		/// <summary>
		/// Radius of the entire crater in heightmap pixels.
		/// </summary>
        protected int CraterRadius { get; private set; }

		/// <summary>
		/// Radius of the "hole" - the middle part in heightmap pixels.
		/// </summary>
		protected int HoleRadius { get; private set; }

		/// <summary>
		/// Normalized height of the walls.
		/// </summary>
		protected float WallsHeight { get; private set; }

		/// <summary>
		/// Normalized depth of the hole.
		/// </summary>
		/// <value>The hole depth.</value>
		protected float HoleDepth { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Habitat.DynamicTerrain.Deformation.CraterDeformation"/> class.
		/// </summary>
		/// <param name="craterRadius">Entire crater radius.</param>
		/// <param name="holeRadius">Hole radius.</param>
		/// <param name="wallsHeight">Normalized walls height.</param>
		/// <param name="holeDepth">Normalized hole depth.</param>
        public CraterDeformation(int craterRadius, int holeRadius, float wallsHeight, float holeDepth)
			: base(craterRadius * 2, craterRadius * 2)
        {
			CraterRadius = craterRadius;
			HoleRadius = holeRadius;
			WallsHeight = wallsHeight;
			HoleDepth = holeDepth;
        }

		/// <summary>
		/// Retrieves the heightmap height value based on the distance to the center.
		/// </summary>
		private float GetDepth(float distanceToCenter)
        {
			if (distanceToCenter <= HoleRadius)
            {
				return (Mathf.Pow(distanceToCenter, 2) * (HoleDepth + WallsHeight) / Mathf.Pow(HoleRadius, 2)) - HoleDepth;
            } else if (distanceToCenter <= CraterRadius) {
				return Mathf.Pow(CraterRadius - distanceToCenter, 2) * WallsHeight / Mathf.Pow(CraterRadius - HoleRadius, 2);
			} else return 0f;
        }

		/// <summary>
		/// Generates the heightmap matrix based on constructor parameters.
		/// </summary>
        public override TerrainDeformation Generate()
        {
            for (var x = 0; x < W; x++)
            {
                for (var y = 0; y < H; y++)
                {
                    var offsetX = x - W / 2;
                    var offsetY = y - H / 2;
                    var depth = GetDepth(Mathf.Sqrt(offsetX * offsetX + offsetY * offsetY));
					Heightmap[x, y] = depth;
                }
            }

            return this;
        }
    }
}
