﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Habitat.DynamicTerrain.Procedural {
	public class Generator
	{
		/// <summary>
		/// Square map dimensions.
		/// </summary>
		private int size;

		/// <summary>
		/// The max coordinate value.
		/// </summary>
		private int max;

		/// <summary>
		/// Heightmap of the game area.
		/// </summary>
		private float[,] map;

		/// <summary>
		/// Determines the maximum elevation change between the two points in the result terrain.
		/// </summary>
		private float roughness;

		/// <summary>
		/// The amount of smooth passes.
		/// </summary>
		private int smoothPasses;

		/// <summary>
		/// Smoothing matrix containing the weights of adjacent values
		/// </summary>
		private float[,] smoothingMatrix = {	
			{0.1111f, 0.1111f, 0.1111f},
			{0.1111f, 0.1111f, 0.1111f},
			{0.1111f, 0.1111f, 0.1111f}
		};
	
		/// <summary>
		/// Initializes a new instance of the <see cref="Habitat.DynamicTerrain.Procedural.Generator"/> class.
		/// </summary>
		/// <param name="detail">Detail level. The result is going to be 2^detail + 1 pixels in size.</param>
		/// <param name="roughnessFactor">Roughness factor. Determines the maximum elevation change between the two points.</param>
		/// <param name="smoothPasses">Amount of smoothing passes.</param>
		public Generator(int detail, float roughnessFactor, int smoothPasses)
		{
			size = (int)Mathf.Pow(2, detail * 1f) + 1;
			max = size - 1;
			map = new float[size, size];
			roughness = roughnessFactor;
			this.smoothPasses = smoothPasses;
		}

		#region [ Set and get methods ]
		private void SetHeight(int x, int y, float val)
		{
			map[x, y] = val;
		}
	
		private float GetHeight(int x, int y)
		{
			if (x < 0 || y < 0 || x > max || y > max) return -1;
			return map[x, y];
		}
	    #endregion

		/// <summary>
		/// Generate the terrain heightmap.
		/// </summary>
		public float[,] Generate()
		{
			SetHeight(0, 0, 0.5f);
			SetHeight(max, 0, 0.5f);
			SetHeight(max, max, 0.5f);
			SetHeight(0, max, 0.5f);

			Divide(max, roughness);
			Smooth(smoothPasses);

			return map;
		}

		/// <summary>
		/// Recursively generates the heights of the terrain using square-diamond algorithm.
		/// </summary>
		/// <param name="stepSize">Square side.</param>
		/// <param name="iterationRoughness">Roughness factor of the current iteration.</param>
		private void Divide(int stepSize, float iterationRoughness)
		{
			int x, y;
			int half = stepSize / 2;

			if (half < 1)
				return;
		
			for (y = half; y < max; y += stepSize)
			{
				for (x = half; x < max; x += stepSize)
				{
					var factor = Random.value * iterationRoughness * 2 - iterationRoughness;
					Square(x, y, half, factor);
				}
			}
		
			for (y = 0; y <= max; y += half)
			{
				for (x = (y + half) % stepSize; x <= max; x += stepSize)
				{
					var factor = Random.value * iterationRoughness * 2 - iterationRoughness;
					Diamond(x, y, half, factor);
				}
			}
		
			Divide(stepSize / 2, iterationRoughness / 2);
		}

		/// <summary>
		/// Sets the coordinates in the corners of the square at (x, y)
		/// </summary>
		/// <param name="x">The x coordinate of the square center.</param>
		/// <param name="y">The y coordinate of the square center.</param>
		/// <param name="size">Size of the square side.</param>
		/// <param name="offset">Elevation change relative to the average of sides.</param>
		private void Square(int x, int y, int size, float offset)
		{
			float[] value = {
				GetHeight(x - size, y - size),
				GetHeight(x + size, y - size),
				GetHeight(x + size, y + size),
				GetHeight(x - size, y + size)
			};
			var average = value.Where(elem => elem != -1).Average();
			SetHeight(x, y, average + offset);
		}

		/// <summary>
		/// Sets the coordinates in the corners of the diamond at (x, y)
		/// </summary>
		/// <param name="x">The x coordinate of the diamond center.</param>
		/// <param name="y">The y coordinate of the diamond center.</param>
		/// <param name="size">Size of the diamond side.</param>
		/// <param name="offset">Elevation change relative to the average of sides.</param>
		private void Diamond(int x, int y, int size, float offset)
		{
			float[] value = {
				GetHeight(x, y - size),
				GetHeight(x + size, y),
				GetHeight(x, y + size),
				GetHeight(x - size, y)
			};
			var average = value.Where(elem => elem != -1).Average();
			SetHeight(x, y, average + offset);
		}

		/// <summary>
		/// Retrieves the smoothed value at x, y using the smoothing matrix weights.
		/// </summary>
		/// <param name="x">The x coordinate to get smoothed value at.</param>
		/// <param name="y">The y coordinate to get smoothed value at.</param>
		private float GetSmoothedValueAt(int x, int y) {
			float smoothedValue = 0f;

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					var heightVal = GetHeight(x + i - 1, y + j - 1);
					if (heightVal == -1) heightVal = GetHeight(x, y);
					smoothedValue += heightVal * smoothingMatrix[i, j];
				}
			}

			return smoothedValue;
		}

		/// <summary>
		/// Applies smoothing to the heightmap, smoothing out the jagginess of the edges.
		/// </summary>
		/// <param name="times">Amount of times to apply the smoothing.</param>
		void Smooth(int times)
		{
			for (int pass = 0; pass < times; pass++)
				for (int x = 0; x < size; x++)
					for (int y = 0; y < size; y++)
						SetHeight(x, y, GetSmoothedValueAt(x, y));
		}
	}
}