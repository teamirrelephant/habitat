﻿using UnityEngine;
using System.Collections;
using Habitat.DynamicTerrain.Deformation;
using Habitat.Game;
using Habitat.DataStructures;

namespace Habitat.DynamicTerrain {

	public class TerrainController : MonoBehaviour {
		public static SuperTerrain Terrain { get; set; }

		public static TerrainController Instance;

		private void Start() {
			Instance = this;
		}
	}
}