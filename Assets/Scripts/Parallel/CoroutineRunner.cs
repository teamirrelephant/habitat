﻿using UnityEngine;
using System.Collections;

namespace Habitat.Parallel {

	public class CoroutineRunner : MonoBehaviour {
		public static CoroutineRunner Instance { get; private set; }

		public delegate IEnumerator CoroutineJob<ArgType>(ArgType argument);

		/// <summary>
		/// Attaches the coroutine to CoroutineRunner game object.
		/// </summary>
		/// <returns>The coroutine.</returns>
		/// <param name="job">Job method.</param>
		/// <param name="argument">Job argument.</param>
		/// <typeparam name="ArgType">Job type parameter</typeparam>
		public Coroutine AttachCoroutine<ArgType>(CoroutineJob<ArgType> job, ArgType argument) {
			return StartCoroutine(job(argument));
		}

		void Start() {
			CoroutineRunner.Instance = this;
		}
	}
}
