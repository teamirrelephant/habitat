namespace Habitat.DataStructures {

	[System.Serializable]
	/// <summary>
	/// Defines the gamezone bounding box which game camera cannot leave.
	/// </summary>
	public struct FloatAABB {
		public float minX;
		public float maxX;
		public float minY;
		public float maxY;
	}
}