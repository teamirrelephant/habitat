namespace Habitat.DataStructures {
    public enum DiplomacyState {
        Friendly,
        Neutral,
        NeutralHostile,
        Hostile
    }    
}