﻿using UnityEngine;
using System.Collections;

namespace Habitat.DataStructures {
	
	public struct DeformationData
	{
		/// <summary>
		/// Heightmap-X coordinate to apply deformation to.
		/// </summary>
		public int X { get; set; }

		/// <summary>
		/// Heightmap-Y coordinate to apply deformation to.
		/// </summary>
		public int Y { get; set; }

		/// <summary>
		/// TerrainData Object to apply deformation to.
		/// </summary>
		public TerrainData TD { get; set; }
	}
}