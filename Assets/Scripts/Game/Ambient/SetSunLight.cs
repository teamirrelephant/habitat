﻿using UnityEngine;
using System.Collections;

namespace Habitat.Game.Ambient {
	public class SetSunLight : MonoBehaviour {
		public Transform Stars;
		
		public Transform WorldProbe;
		
		void Update () {
			Stars.transform.rotation = transform.rotation;
			WorldProbe.transform.position = Camera.main.transform.position;
		}
	}
}