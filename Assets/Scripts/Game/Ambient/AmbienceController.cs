using UnityEngine;

namespace Habitat.Game.Ambient {
	public class AmbienceController : MonoBehaviour {

		public static AmbienceController Instance { get; private set; }

		public GameObject Sky { get; private set; }

		public GameObject BasicLight { get; private set; }

		public void UpdateAmbienceBasedOnGameSettings() {
			Sky.SetActive (!GameSettings.NoDayNight);
			BasicLight.SetActive (GameSettings.NoDayNight);
		}

		private void Start() {
			Instance = this;
			Sky = transform.GetChild (1).gameObject;
			BasicLight = transform.GetChild (2).gameObject;
			UpdateAmbienceBasedOnGameSettings ();
		}
	}
}