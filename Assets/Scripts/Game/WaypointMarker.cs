﻿using UnityEngine;
using System.Collections;

public class WaypointMarker : MonoBehaviour {
	void Start () {
		StartCoroutine (DieAfterThreeSeconds ());
	}

	IEnumerator DieAfterThreeSeconds() {
		yield return new WaitForSeconds (0.5f);
		Destroy (gameObject);
	}
}
