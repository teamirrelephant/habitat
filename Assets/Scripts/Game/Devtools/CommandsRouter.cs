﻿using UnityEngine;
using System.Collections.Generic;

using Habitat.Game.Devtools.Commands;

namespace Habitat.Game.Devtools
{
		/// <summary>
		/// Routes available console commands.
		/// </summary>
	public class CommandsRouter : MonoBehaviour 
	{
		public static readonly List<IConsoleCommand> cmdList = new List<IConsoleCommand>
		{
			new Help(),
			new SpawnUnitByName(),
			new NetGraph(),
			new ToggleDaynight(),
			new SetTimeFactor(),
			new SpawnCrater(),
			new TestAnimatedDeformation()
		};

		void Start () 
		{
			var cons = ConsoleCommandsRepository.Instance;
			cmdList.ForEach(cmd => {
			    foreach (var alias in cmd.Names) {
					cons.RegisterCommand (alias, cmd.ToExecute);
				}
			});
		} 
	}
}
