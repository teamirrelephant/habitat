namespace Habitat.Game.Devtools.Commands {
	public class NetGraph : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "cl_netgraph" });
			}
		}
		
		public string HelpString
		{
			get {
				return "toggles the net graph (fps view)";
			}
		}
		
		public string ToExecute(params string[] args)
		{
			GameSettings.ShowNetGraph = !GameSettings.ShowNetGraph;
			return "net graph: " + GameSettings.ShowNetGraph;
		}
	}
}