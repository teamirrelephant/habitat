using UnityEngine;
using Habitat.Game.Ambient;

namespace Habitat.Game.Devtools.Commands {
	public class ToggleDaynight : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "sv_toggle_daynight" });
			}
		}
		
		public string HelpString
		{
			get {
				return "toggles the daynight cycle advanced lighting.";
			}
		}
		
		public string ToExecute(params string[] args)
		{
			GameSettings.NoDayNight = !GameSettings.NoDayNight;
			AmbienceController.Instance.UpdateAmbienceBasedOnGameSettings ();
			return "daynight: " + GameSettings.NoDayNight;
		}
	}
}