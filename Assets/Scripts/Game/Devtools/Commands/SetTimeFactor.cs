using Habitat.Game;
using System;
using UnityEngine;

namespace Habitat.Game.Devtools.Commands
{
	public class SetTimeFactor : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "sv_timefactor" });
			}
		}
		
		public string HelpString
		{
			get {
				return "Controls the game time speed";
			}
		}
		
		public string ToExecute(params string[] args)
		{
			try {
				var timeFactor = float.Parse(args[0]);
				GameSettings.TimeFactor = timeFactor;
				return string.Format("TimeFactor: {0}", timeFactor);
			}
			catch (Exception) {
				return string.Format("TimeFactor: {0}", GameSettings.TimeFactor);
			}
		}
	}
}
