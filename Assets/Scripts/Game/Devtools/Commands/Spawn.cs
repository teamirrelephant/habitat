using Habitat.Game;
using System;
using UnityEngine;

namespace Habitat.Game.Devtools.Commands
{
	public class SpawnUnitByName : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "spawn" });
			}
		}

		public string HelpString
		{
			get {
				return "Spawns unit by it's name. args: name x y";
			}
		}

		public string ToExecute(params string[] args)
		{
			try {
				var coords = new Vector2(float.Parse(args[1]), float.Parse(args[2]));
				if (GameController.Instance.SpawnUnitByName(args[0], coords) == null) {
					return "No such unit found.";
				}
				return string.Format("Succesfully spawned {0} at {1}", args[0], args[1]);
			}
			catch (FormatException) {
				return "Format exception: Either x or y does not represent a number in a valid format";
			}
			catch (Exception e) {
				return "Shit happened... " + e.Message + "\n try and man this command";
			}
		}
	}
}
