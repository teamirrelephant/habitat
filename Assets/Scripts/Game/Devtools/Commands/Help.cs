using System.Linq;

namespace Habitat.Game.Devtools.Commands
{
	public class Help : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "help", "man" });
			}
		}

		public string HelpString
		{
			get {
				return "Show this help";
			}
		}
		
		public string ToExecute(params string[] args)
		{
			if(args.Length == 1)
				return GetSingleCommandHelp(args[0]);
			return GetAllCommandsHelp();
		}

		#region [ Private methods ]

		private string GetSingleCommandHelp (string cmdName)
		{
			IConsoleCommand cmd = CommandsRouter.cmdList.Find (c => c.Names.Any(s => s == cmdName));
			if (cmd == null)
				return string.Format ("No \'{0}\' command", cmdName);
			return MakeCmdHelpString(cmd);
		}

		private string GetAllCommandsHelp ()
		{
			string result = "";
			CommandsRouter.cmdList.ForEach (cmd => result += MakeShortHelpString(cmd));
			return result;
		}

		private string MakeCmdHelpString(IConsoleCommand cmd)
		{
			var aliases = "Aliases:\n";
			foreach (var alias in cmd.Names) {
				aliases += alias + '\n';
			}
			return string.Format("{0} - {1}\n", aliases, cmd.HelpString);
		}

		private string MakeShortHelpString(IConsoleCommand cmd) {
			return string.Format("{0} - {1}\n", cmd.Names.FirstOrDefault(), cmd.HelpString);
		}

		#endregion
	}
}
