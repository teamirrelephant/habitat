using Habitat.Game;
using System;
using UnityEngine;
using Habitat.DynamicTerrain;
using Habitat.DynamicTerrain.Deformation;

namespace Habitat.Game.Devtools.Commands
{
	public class SpawnCrater : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "spawn_crater" });
			}
		}
		
		public string HelpString
		{
			get {
				return "Spawns crater deformation. args: x y radius hole_radius normalized_depth normalized_wall_height";
			}
		}
		
		public string ToExecute(params string[] args)
		{
			try {
				var x = int.Parse(args[0]);
				var y = int.Parse(args[1]);
				var radius = int.Parse(args[2]);
				var hole = int.Parse(args[3]);
				var depth = float.Parse(args[4]);
				var wall = float.Parse(args[5]);

				TerrainController.Terrain.ApplyDeformation(new CraterDeformation(
					radius,
					hole,
					wall,
					depth).Generate(), x, y);

				return string.Format("Success!");
			}
			catch (FormatException) {
				return "Format exception: Some of the numeric arguments does not represent a number in a valid format";
			}
			catch (Exception e) {
				Debug.Log("Shit happened");
				return "Shit happened... " + e.Message + "\n try and man this command";
			}
		}
	}
}
