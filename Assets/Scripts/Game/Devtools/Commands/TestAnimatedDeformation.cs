using System;
using System.Collections;
using UnityEngine;

using Habitat.Game;
using Habitat.DynamicTerrain;
using Habitat.DynamicTerrain.Deformation;
using Habitat.Parallel;

namespace Habitat.Game.Devtools.Commands
{
	public class TestAnimatedDeformation : IConsoleCommand
	{
		private string[] names;
		public string[] Names
		{
			get {
				return names ?? (names = new[] { "sv_spawn_animdef" });
			}
		}
		
		public string HelpString
		{
			get {
				return "Spawns test animated deformation at the center of the map.";
			}
		}

		private IEnumerator CoroutineJob(int iterations) {
			int iterationsRemaining = iterations;

			Vector2 currentPoint = new Vector3(128, 128);
			Vector2 currentDirection = UnityEngine.Random.insideUnitCircle;
			var deformation = new CraterDeformation (5, 5, 0, 0.003f).Generate();
			while (iterationsRemaining-- > 0) {
				TerrainController.Terrain.ApplyDeformation(deformation, currentPoint.x, currentPoint.y);
				currentPoint += (currentDirection);
				currentDirection += new Vector2(UnityEngine.Random.Range(0f, 0.5f), UnityEngine.Random.Range(0f, 0.5f));
				currentDirection.Normalize();
				yield return new WaitForSeconds(0.1f);
			}
		}

		public string ToExecute(params string[] args)
		{
			CoroutineRunner.Instance.AttachCoroutine (CoroutineJob, 50);
			return "Coroutine attached.";
		}
	}
}
