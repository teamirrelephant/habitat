﻿using UnityEngine;
using System.Collections;
using System.Linq;

using Habitat.DynamicTerrain.Procedural;
using Habitat.DynamicTerrain;
using Habitat.DynamicTerrain.Deformation;

namespace Habitat.Game {

	public class TerrainGenerator : MonoBehaviour
	{
		public int Detail;

		public float Roughness;

		public int Smoothing;

		public int SuperterrainDetail;

		public Transform TerrainContainer;

		void Start()
		{
			var terrain = new SuperTerrain(SuperterrainDetail, TerrainContainer); 
			var gen = new Generator(Detail, Roughness, Smoothing);
			var heightMap = gen.Generate();
			terrain.SetGlobalHeightmap(heightMap);
			AstarPath.active.Scan();
			TerrainController.Terrain = terrain;
			GameController.Instance.OnTerrainReady();
		}
	}
}