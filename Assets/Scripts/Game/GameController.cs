using System;
using UnityEngine;
using System.Linq;

using Habitat.Game.Player;
using Habitat.Game.Helper;
using Habitat.Game.AI.Unit.Controller;

namespace Habitat.Game
{
	[Serializable]
	public struct UnitEntry {
		public string Name;
		public bool IsBuilding;
		public GameObject Unit;
	}

	/// <summary>
	/// Keeps track of available unit types and can spawn new units ingame.
	/// </summary>
	public class GameController : MonoBehaviour
	{
		#region [ Imported variables ]
		/// <summary>
		/// Singleton instance getter property
		/// </summary>
		/// <value>The instance.</value>
		public static GameController Instance { get; private set; }

		public PlayerInfo CurrentPlayer { get; private set; }

		/// <summary>
		/// Pseudo-dictionary of available units
		/// </summary>
		public UnitEntry[] Units;

		/// <summary>
		/// Gameobject to spawn new units into
		/// </summary>
		public GameObject UnitContainer;

		#endregion

		#region [ Private methods ]

		/// <summary>
		/// Retrieves the terrain height in the given point using raycast.
		/// </summary>
		/// <returns>The terrain height.</returns>
		/// <param name="where">Where.</param>
		private float GetTerrainHeight (Vector2 where)
		{
			RaycastHit hit;
			Physics.Raycast (new Vector3 (where.x, 0f, where.y),
			                 transform.up,
			                 out hit,
			                 GameplayConstants.WorldHeight + GameplayConstants.RaycastingDelta,
			                 GameplayConstants.TerrainLayerMask);
			return hit.point.y;
		}
		
		#endregion

		#region [ GameObject interfacing ]

		void Start() {
			Instance = this;
			CurrentPlayer = new PlayerInfo("Developer", new Color (0.49f, 0f, 1f));
		}

		#endregion

		#region [ Public methods ]

		/// <summary>
		/// raised when the terrain generator has done generating new terrain.
		/// </summary>
		public void OnTerrainReady() {
			SpawnUnitByName("TestTank", new Vector2(128f, 128f));
			SpawnUnitByName("TestTank", new Vector2(148f, 148f));
			SpawnUnitByName("TestBuilding", new Vector2(100f, 100f));

			SpawnUnitByName("TestBuilding", new Vector2(160f, 160f), PlayerInfo.EWIC);
			SpawnUnitByName("TestBuilding", new Vector2(170f, 165f), PlayerInfo.EWIC);
			SpawnUnitByName("TestBuilding", new Vector2(180f, 170f), PlayerInfo.EWIC);
			SpawnUnitByName("TestTank", new Vector2(138f, 138f), PlayerInfo.EWIC);
		}

		/// <summary>
		/// Spawns new unit from a given prefab.
		/// </summary>
		/// <returns>The unit spawned.</returns>
		/// <param name="unitPrefab">Unit prefab.</param>
		/// <param name="where">Where to spawn the unit.</param>
		private GameObject SpawnUnit(GameObject unitPrefab, Vector2 where, PlayerInfo owner, bool isBuilding) {
			var exactPosition = new Vector3 (where.x, GetTerrainHeight (where), where.y);
			var unit = (GameObject)GameObject.Instantiate(unitPrefab, exactPosition, Quaternion.identity);
			unit.transform.parent = UnitContainer.transform;
			var controller = unit.GetComponent<EntityController>();
			if (controller)
			{
				controller.Owner = owner;
				GameObjectHelper.SetLayerRecursive(controller.gameObject, isBuilding ? GameplayConstants.BuildingsLayer : GameplayConstants.UnitsLayer);
			}
			return unit;
		}

		/// <summary>
		/// Spawns the unit by its name in the public pseudodictionary.
		/// </summary>
		/// <returns>The spawned unit or null if not found.</returns>
		/// <param name="name">Name of the unit.</param>
		/// <param name="where">Where to spawn the unit.</param>
		public GameObject SpawnUnitByName(string name, Vector2 where, PlayerInfo owner = null) {
			var entry = Units.FirstOrDefault(u => u.Name.Equals(name));
			if (!entry.Equals(default(UnitEntry))) {
				return SpawnUnit(entry.Unit, where, owner ?? GameController.Instance.CurrentPlayer, entry.IsBuilding);
			} else {
				Debug.LogWarningFormat("Attempting to spawn unit by its name: {0}. No such unit registered.",  name);
				return null;
			}
		}
		
		#endregion
	}
}

