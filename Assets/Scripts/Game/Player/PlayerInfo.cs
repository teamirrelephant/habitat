using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Habitat.DataStructures;
using Habitat.Game.Helper;
using System;

namespace Habitat.Game.Player {

	/// <summary>
	/// Incapsulates information about the player and serves to associate units to certain player.
	/// </summary>
	public class PlayerInfo {

		#region [ Predefined Players and Player Accessors ]

		public static PlayerInfo EWIC = new PlayerInfo("E.W.I.C.", new Color (1f, 0.68f, 0.2f));
		public static PlayerInfo Aliens = new PlayerInfo("Alien Hive", new Color (0.35f, 0.2f, 0.2f));
        
		/// <summary>
		/// Internal players container
		/// </summary>
        private static IDictionary<String, PlayerInfo> players = new Dictionary<String, PlayerInfo>();
          
		/// <summary>
		/// Retrieves the player by his name. Returns null if players doesn't exist.
		/// </summary>
        public static PlayerInfo GetPlayerByName(string name) {
            if (players.ContainsKey(name)) return players[name];
            return null;            
        }
        
		/// <summary>
		/// Registers the player in the player dictionary.
		/// </summary>        
        public static void RegisterPlayer(PlayerInfo p) {
            players[p.Name] = p;
        }
        
		/// <summary>
		/// Unregisters the player from the player dictionary.
		/// </summary>        
        public static void ForgetPlayer(PlayerInfo p) {
            players[p.Name] = null;
        } 
        
		/// <summary>
		/// Lists all players.
		/// </summary>
        public IEnumerable AllPlayers { get { return players; } }
        
		#endregion

        #region [ Diplomacy ]
        
        private IDictionary<PlayerInfo, DiplomacyState> diplomacyInfo = new Dictionary<PlayerInfo, DiplomacyState>();
        
		/// <summary>
		/// Returns the attitude of one player towards another
		/// </summary>
        public DiplomacyState GetAttitudeTowards(PlayerInfo p) {
            if (diplomacyInfo.ContainsKey(p)) {
                return diplomacyInfo[p];
            } else {
                return DiplomacyState.NeutralHostile;
            }
        }
        
		/// <summary>
		/// Sets the attitude of players towards each other. Takes
        /// into consideration target player's attitude towards the current
        /// player.
		/// </summary>
        public void SetAttitudeTowards(PlayerInfo p, DiplomacyState state) {
            diplomacyInfo[p] = state;
            if (!p.diplomacyInfo[this].IsWorseThan(state)) {
                p.diplomacyInfo[this] = state;
            }
        }
        
        #endregion
        
        
		/// <summary>
		/// Player's in-game display name.
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }

		/// <summary>
		/// Faction color for the units of the player.
		/// </summary>
		/// <value>The color.</value>
		public Color Color { get; set; }

		/// <summary>
		/// Indicates whether this player is controlled by AI.
		/// </summary>
		/// <value><c>true</c> if AI controlled; otherwise, <c>false</c>.</value>
		public bool AIControlled { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Habitat.Game.Player"/> class.
		/// </summary>
		/// <param name="name">Player display name.</param>
		/// <param name="color">Faction color.</param>
		/// <param name="aiControlled">Enables the AI control for this player.</param>
		public PlayerInfo(string name, Color color, bool aiControlled = false) {
			Name = name;
			Color = color;
			AIControlled = aiControlled;
		}
	}
}