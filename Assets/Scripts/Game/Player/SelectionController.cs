using System.Collections.Generic;
using System.Linq;

using Habitat.Game.AI.Unit;
using Habitat.Game.AI.Unit.Controller;
using Habitat.Game.AI.Unit.Behaviour;

namespace Habitat.Game.Player {
	public class SelectionController {
		private List<SelectableEntity> selected;

		public IEnumerable<EntityController> Selected {
			get {
				return selected.Select(s => s.Entity);
			}
		}

		public SelectionController() {
			selected = new List<SelectableEntity>();
		}

		public void SelectSingle(SelectableEntity target) {
			ClearSelection();
			AddToSelection(target);
		}

		public void DeselectSingle(SelectableEntity target) {
			target.Deselect();
			selected.Remove(target);
		}

		public void AddToSelection(SelectableEntity target) {
			target.Select();
			selected.Add(target);
		}

		public bool IsSelected(SelectableEntity target) {
			return selected.Contains(target);
		}

		public void ToggleSelection(SelectableEntity target) {
			if (IsSelected (target)) {
				DeselectSingle (target);
			} else {
				AddToSelection(target);
			}
		}

		public void ClearSelection() {
			selected.ForEach(s => s.Deselect ());
			selected.Clear();
		}
	}
}
