﻿using UnityEngine;
using System.Collections;

using Habitat.DataStructures;

namespace Habitat.Game.Player.Camera {

	public class CameraMover : MonoBehaviour {
		/// <summary>
		/// Instance of the camera mover.
		/// </summary>
		/// <value>The instance.</value>
		public static CameraMover Instance { get; private set; }
		
		/// <summary>
		/// Camera max move speed per second.
		/// </summary>
		public float MoveSpeed = 50f;
		
		/// <summary>
		/// X-axis sensitivity when rotating the camera with "Rotate" button.
		/// </summary>
		public float SensitivityX = 2;
		
		/// <summary>
		/// Y-axis sensitivity when rotating the camera with "Rotate" button.
		/// </summary>
		public float SensitivityY = 2;
		
		/// <summary>
		/// The height of the camera above the ground surface.
		/// </summary>
		public float CameraHeight = 50f;
		
		/// <summary>
		/// Bounding box for the game map dimensions.
		/// </summary>
		public FloatAABB GamezoneAABB;
		
		/// <summary>
		/// Screen bounding box (normalized values) for mouse camera movement.
		/// </summary>
		public FloatAABB CameraMoverAABB;
		
		/// <summary>
		/// Contains current rotation angle around the Y axis.
		/// </summary>
		private float rotationY = -55f;
		
		/// <summary>
		/// Camera acceleration when using mouse.
		/// </summary>
		private float acceleration = 0.3f;
		
		/// <summary>
		/// Contains current mouse scrolling velocity along X axis.
		/// </summary>
		private float mouseVelocityX = 0f;
		
		/// <summary>
		/// Contains current mouse scrolling velocity along Y axis.
		/// </summary>
		private float mouseVelocityY = 0f;
		
		/// <summary>
		/// Determines the current camera elevation percentage relative to the maximum.
		/// </summary>
		private float currentZoom = 1f;


		private void Start() {
			Instance = this;
			transform.Rotate(new Vector3(-rotationY, 0f, 0f), Space.World);
		}

		/// <summary>
		/// Retrieves the terrain height under the current camera position
		/// </summary>
		private float GetCurrentTerrainHeight() {
			RaycastHit hit;
			if (Physics.Raycast(transform.position, -Vector3.up, out hit, GameplayConstants.WorldHeight, GameplayConstants.TerrainLayerMask)) {
				return hit.point.y;
			} else
				return GameplayConstants.WorldHeight;
		}

		/// <summary>
		/// Updates the camera position from WASD movement.
		/// </summary>
		private void UpdateCameraPositionWithKeyboard() {
			var forwardSpeed = transform.forward;
			forwardSpeed.y = 0;
			forwardSpeed.Normalize();
			forwardSpeed *= Input.GetAxis("Vertical");
			var rightSpeed = Input.GetAxis("Horizontal");
			
			var speed = (forwardSpeed + transform.right * rightSpeed) * MoveSpeed;
			transform.Translate(speed * Time.deltaTime, Space.World);
		}

		/// <summary>
		/// Retrieves forward movement induced by the mouse position.
		/// </summary>
		/// <returns>The forward movement vector.</returns>
		private Vector3 GetForwardMovementVector() {
			var normalizedY = Input.mousePosition.y / Screen.height;
			var forwardSpeed = transform.forward;
			forwardSpeed.y = 0;
			forwardSpeed.Normalize();
			
			if (normalizedY < CameraMoverAABB.minY) {
				mouseVelocityY = Mathf.Lerp(mouseVelocityY, -1f, acceleration);
			} else if (normalizedY < CameraMoverAABB.maxY) {
				mouseVelocityY = Mathf.Lerp(mouseVelocityY, 0f, acceleration);
			} else {
				mouseVelocityY = Mathf.Lerp(mouseVelocityY, 1f, acceleration);        
			}
			
			return forwardSpeed * mouseVelocityY;
		}	

		/// <summary>
		/// Retrieves right movement induced by the mouse position.
		/// </summary>
		/// <returns>The right movement vector.</returns>
		private Vector3 GetRightMovementVector() {
			var normalizedX = Input.mousePosition.x / Screen.width;
			var rightSpeed = transform.right;
			
			if (normalizedX < CameraMoverAABB.minX) {
				mouseVelocityX = Mathf.Lerp(mouseVelocityX, -1f, acceleration);
			} else if (normalizedX < CameraMoverAABB.maxX) {
				mouseVelocityX = Mathf.Lerp(mouseVelocityX, 0f, acceleration);
			} else {
				mouseVelocityX = Mathf.Lerp(mouseVelocityX, 1f, acceleration);        
			}
			
			return rightSpeed * mouseVelocityX;
		}
		
		/// <summary>
		/// Updates the camera position when mouse touches the screen boundary.
		/// </summary>
		private void UpdateCameraPositionWithMouse() {
			var speed = (GetForwardMovementVector() + GetRightMovementVector()) * MoveSpeed;
			transform.Translate(speed * Time.deltaTime, Space.World);
		}
		
		/// <summary>
		/// Locks the mouse when rotation is enabled.
		/// </summary>
		private void LockMouseIfNeeded ()
		{
			if (Input.GetButtonDown("Rotate")) {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			} else if (Input.GetButtonUp("Rotate")) {
				Cursor.lockState = CursorLockMode.Confined;
				Cursor.visible = true;
			}
		}
		
		/// <summary>
		/// Updates the camera rotation when the "Rotate" button is pressed.
		/// </summary>
		private void UpdateCameraRotation() {
			LockMouseIfNeeded();
			if (Input.GetButton("Rotate")) {
				float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * SensitivityX;
				rotationY = Mathf.Clamp(rotationY + Input.GetAxis("Mouse Y") * SensitivityY, -89f, 89f);
				transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
			}
		}
		
		/// <summary>
		/// Clamps the camera within the game area.
		/// </summary>
		private void EnsureCameraPositionWithinGamezone()
		{
			transform.position = new Vector3(Mathf.Clamp(transform.position.x, GamezoneAABB.minX, GamezoneAABB.maxX),
			                                 transform.position.y,
			                                 Mathf.Clamp(transform.position.z, GamezoneAABB.minY, GamezoneAABB.maxY));
			
		}
		
		/// <summary>
		/// Updates the height of the camera based on the terrain height underneath.
		/// </summary>
		private void UpdateCameraHeight() {
			transform.Translate(new Vector3(0f, Mathf.Lerp(0, GetCurrentTerrainHeight() + CameraHeight * currentZoom - transform.position.y, 0.1f), 0f), Space.World);
		}
		
		/// <summary>
		/// Updates the camera zoom using the mouse wheel input.
		/// </summary>
		private void UpdateCameraZoom ()
		{
			currentZoom = Mathf.Clamp(-Input.GetAxis("Mouse ScrollWheel") + currentZoom, 0.3f, 1f); 
		}

		/// <summary>
		/// Update the camera position.
		/// </summary>
		private void Update() {
			UpdateCameraPositionWithKeyboard();
			UpdateCameraPositionWithMouse();
			UpdateCameraZoom();
			UpdateCameraHeight();
			UpdateCameraRotation();
			EnsureCameraPositionWithinGamezone();
		}
	}
}
