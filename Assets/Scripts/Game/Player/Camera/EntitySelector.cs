﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

using Habitat.Game.AI.Unit;
using Habitat.Game.Helper;
using Habitat.Game.AI.Unit.Controller;
using Habitat.Game.AI.Unit.Behaviour;

namespace Habitat.Game.Player.Camera {

	public class EntitySelector : MonoBehaviour {
		/// <summary>
		/// Instance of the entity selector
		/// </summary>
		/// <value>The instance.</value>
		public static EntitySelector Instance { get; private set; }

		/// <summary>
		/// Determines whether box selction process in in progress.
		/// </summary>
		/// <value><c>true</c> if box selection in progress; otherwise, <c>false</c>.</value>
		public bool BoxSelectionInProgress { get; private set; }
		
		/// <summary>
		/// Box selection rectangle. Any unit owned by current player that is caught in this
		/// box while selecting is going to end up selected.
		/// </summary>
		/// <value>The box selection rectangle.</value>
		private Rect selectionRectangle;
		public Rect BoxSelectionRectangle {
			get { return selectionRectangle; }
		}
		
		/// <summary>
		/// Selection controller. Contains all the information about current unit selection.
		/// </summary>
		private SelectionController selector;
		
		/// <summary>
		/// Selection texture
		/// </summary>
		public Texture selectionHighlight;
		
		/// <summary>
		/// The selection origin - screen point to begin box selection at.
		/// </summary>
		private Vector2 selectionOrigin;
		
		/// <summary>
		/// Controls the minimal rectangle diagonal to start box selection.
		/// </summary>
		public float boxSelectionError;

		/// <summary>
		/// Game object which contains all the selectable entities. Used to apply box selections.
		/// </summary>
		public GameObject entityContainer;

		private void Start() {
			selector = new SelectionController();
			Instance = this;
		}

		/// <summary>
		/// This method is raised by the selected unit when box selection is performed.
		/// </summary>
		/// <param name="unit">Unit.</param>
		public void NotifyBoxSelection(SelectableEntity unit) {
			selector.AddToSelection(unit);
		}

		/// <summary>
		/// Sets the box selection origin
		/// </summary>
		public void StartBoxSelection ()
		{
			selectionOrigin = Input.mousePosition;
		}

		/// <summary>
		/// Completes the box selection and sends the selection box message to visible selectable units.
		/// </summary>
		/// <param name="clearCurrentSelection">If set to <c>true</c> clear current selection.</param>
		public void TryEndBoxSelection(bool clearCurrentSelection) {
			if (BoxSelectionInProgress) {
				BoxSelectionInProgress = false;
				FixRectangleInversion ();
				DeselectNonPlayer();
				if (clearCurrentSelection) selector.ClearSelection ();
				entityContainer.BroadcastMessage("OnSelectionBox");
			}
		}

		/// <summary>
		/// Handles box selection.
		/// </summary>
		public void ProcessBoxSelection() {
			if (!BoxSelectionInProgress && (selectionOrigin - new Vector2(Input.mousePosition.x, Input.mousePosition.y)).magnitude > boxSelectionError) {
				BoxSelectionInProgress = true;
			}
			
			selectionRectangle = new Rect(selectionOrigin.x,
			                              WorldSpaceHelper.WorldToScreenSpaceY(selectionOrigin.y),
			                              Input.mousePosition.x - selectionOrigin.x,
			                              WorldSpaceHelper.WorldToScreenSpaceY(Input.mousePosition.y) - WorldSpaceHelper.WorldToScreenSpaceY(selectionOrigin.y));
		}
		
		/// <summary>
		/// Draws the selection GUI if needed.
		/// </summary>
		void OnGUI() {
			if (BoxSelectionInProgress) {
				GUI.color = new Color(1f, 1f, 1f, 0.8f);
				GUI.DrawTexture(selectionRectangle, selectionHighlight);
			}
		}

		/// <summary>
		/// Fixes the selection rectangles which have either negative widths or negative heights
		/// </summary>
		private void FixRectangleInversion()
		{
			if (selectionRectangle.width < 0) {
				selectionRectangle.x += selectionRectangle.width;
				selectionRectangle.width = -selectionRectangle.width;
			}
			if (selectionRectangle.height < 0) {
				selectionRectangle.y += selectionRectangle.height;
				selectionRectangle.height = -selectionRectangle.height;
			}
		}
		
		/// <summary>
		/// Deselects the non player units.
		/// </summary>
		void DeselectNonPlayer()
		{
			var nonPlayer = selector.Selected.FirstOrDefault (u => u.Owner != GameController.Instance.CurrentPlayer);
			if (nonPlayer) selector.DeselectSingle(nonPlayer.GetComponent<SelectableEntity>());
		}

		/// <summary>
		/// Performs the single unit selection/deselection action.
		/// </summary>
		/// <param name="target">Target.</param>
		public void PerformSingleEntitySelection(Transform target, bool toggleSelection) {
			var selectable = target.GetComponent<SelectableEntity>();
			if (selectable) {
				if (toggleSelection)
				{
					DeselectNonPlayer();
					if (selectable.Entity.Owner == GameController.Instance.CurrentPlayer)
						selector.ToggleSelection(selectable);
				}
				else {
					selector.SelectSingle(selectable);
				}		
			}
		}

		/// <summary>
		/// Clears current unit selection
		/// </summary>
		public void ClearSelection() {
			selector.ClearSelection();
		}

		/// <summary>
		/// Contains all the selected units
		/// </summary>
		/// <value>The selected.</value>
		public IEnumerable<EntityController> Selected { 
			get {
				return selector.Selected;
			}
		}
	}
}
