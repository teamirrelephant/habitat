﻿using UnityEngine;
using System.Linq;
using System.Collections;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Game.Helper;

namespace Habitat.Game.Player.Camera {

	public class GameInteractor : MonoBehaviour {
	
		/// <summary>
		/// Detects the "confirm target" order (e.g. attack order target, selection, etc...)
		/// </summary>
		private void DetectConfirmTarget()
		{
			if (!EntitySelector.Instance.BoxSelectionInProgress && Input.GetButtonUp("ConfirmTarget")) {
				var target = RaycastingHelper.RetrieveRoot(RaycastingHelper.GetPointingTarget(GameplayConstants.UnitsLayerMask | GameplayConstants.BuildingsLayerMask));
				if (target) {
					var toggleSelection = Input.GetButton("QueueOrder");
					EntitySelector.Instance.PerformSingleEntitySelection(target.transform, toggleSelection);
				} else EntitySelector.Instance.ClearSelection();
			}
		}

		/// <summary>
		/// Detects the box selection inputs and invokes the appropatiate calls on the EntitySelector.
		/// </summary>
		void DetectBoxSelection()
		{
			if (Input.GetButtonDown ("ConfirmTarget")) {
				EntitySelector.Instance.StartBoxSelection ();
			} else if (Input.GetButtonUp ("ConfirmTarget")) {
				var clearSelection = !Input.GetButton("QueueOrder");
				EntitySelector.Instance.TryEndBoxSelection(clearSelection);
			}

			if (Input.GetButton ("ConfirmTarget")) {
				EntitySelector.Instance.ProcessBoxSelection();
			}
		}

		/// <summary>
		/// Gives the selected units default entity-order
		/// </summary>
		/// <param name="entityContainer">Entity container.</param>
		void PerformDefaultOrderOnEntity(GameObject entityContainer, bool queue)
		{
			//TODO: Contextual order giving. Targeting the hostile units triggers
			//attack order on combat-enabled units, etc...
			foreach (UnitController selected in EntitySelector.Instance.Selected
			         .Where(e => e.Owner == GameController.Instance.CurrentPlayer)
			         .Where(e => e != entityContainer.GetComponent<EntityController>())
			         .OfType<UnitController>()) {

				selected.GiveDefaultEntityOrder(entityContainer, queue);
			}
		}

		/// <summary>
		/// Performs the default order on terrain - the move order.
		/// </summary>
		/// <param name="point">Point on the terrain.</param>
		void PerformDefaultOrderOnTerrain (Vector3 point, bool queue)
		{
			foreach (UnitController selected in EntitySelector.Instance.Selected
			         .Where(e => e.Owner == GameController.Instance.CurrentPlayer)
			         .OfType<UnitController>()) {
				
				selected.GiveDefaultTerrainOrder(point, queue);
			}
		}
	
		/// <summary>
		/// Detects the default order (e.g. move, attack, follow).
		/// </summary>
		private void DetectDefaultOrder() {
			if (Input.GetButtonDown("DefaultOrder")) {
				var target = RaycastingHelper.GetPointingTarget(GameplayConstants.TerrainLayerMask |
				                               GameplayConstants.UnitsLayerMask |
				                               GameplayConstants.BuildingsLayerMask);

				var entityContainer = RaycastingHelper.RetrieveRoot(target);
				if (entityContainer) {
					PerformDefaultOrderOnEntity(entityContainer, Input.GetButton("QueueOrder"));
				} else {
					PerformDefaultOrderOnTerrain(target.point, Input.GetButton("QueueOrder"));
				}
			}
		}

		/// <summary>
		/// Grabs the input from the player and acts accordingly.
		/// </summary>
		private void Update() {
			DetectConfirmTarget();
			DetectBoxSelection();
			DetectDefaultOrder();
		}
	}
}
