namespace Habitat.Game {

	public static class GameSettings {
		/// <summary>
		/// The upper-left corner net-graph data display toggle.
		/// </summary>
		public static bool ShowNetGraph = true; 

		/// <summary>
		/// The upper-left corner net-graph data display toggle.
		/// </summary>
		public static bool NoDayNight = true; 

		/// <summary>
		/// Daynight time factor
		/// </summary>
		public static float TimeFactor = 1f;
	}
}