using UnityEngine;
using System.Collections;

using Pathfinding;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Parallel;

namespace Habitat.Game.AI.Unit.Order
{
	public class IdleOrder : IOrder
	{
		public GameObject Target { get; private set; }

		public UnitController Unit { get; private set; }
        
        public WeaponController Weapon { get; private set; } 

		public IdleOrder(UnitController unit)
		{
			Unit = unit;
            Weapon = Unit.Weapons;
        }

        private float retargetRate = 1f;
        private float lastRetarget = -9999;
        
        private float unreachableLimit = 3f;
        private float unreachableFor = 0f;

        private void PickReachableTarget() {
            var objects = Physics.OverlapSphere(Unit.transform.position, Weapon.maxRange);
            Target = objects.FirstOrDefault(collider => Weapon.IsReachable(collider));
            if (Target != null) Weapon.SetTarget(Target);
            else Weapon.ClearTarget();
        }
        
		public bool Execute()
		{
            if ((Target == null || unreachableFor > unreachableLimit) && Time.time - lastRetarget > retargetRate) {
                PickReachableTarget();
                lastRepath = Time.time + Random.value * retargetRate * 0.5f;
            }
            
            if (Weapon.IsTargetReachable()) {
                unreachableFor = 0;
            } else {
                unreachableFor += Time.deltaTime;
            }
            
            return false;  
		}

		public void Cancel () { }
	}
}
