﻿using System.Collections.Generic;
using System.Linq;

using Habitat.Game.AI.Unit.Order;

namespace Habitat.AI.Unit {
	public class OrderQueue {

		private List<IOrder> orders;

		public OrderQueue() {
			orders = new List<IOrder>();
		}

		public void AddToQueue(IOrder order) {
			orders.Insert(0, order);
		}

		public void OvewriteQueueWithOrder(IOrder order) {
			orders.ForEach((o) => o.Cancel());
			orders.Clear();
			orders.Add(order);
		}

		public void ExecuteCurrentOrder() {
			var order = orders.FirstOrDefault();
			if (order != null) {
				if (order.Execute()) {
					orders.RemoveAt(0);
				}
			}
		}
	}

}