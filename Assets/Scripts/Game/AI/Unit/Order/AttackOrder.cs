using UnityEngine;
using System.Collections;

using Pathfinding;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Parallel;

namespace Habitat.Game.AI.Unit.Order
{
	public class AttackOrder : IOrder
	{
		public GameObject Target { get; private set; }

		public Seeker PathSeeker { get; private set; }

		public UnitController Unit { get; private set; }
        
        public WeaponController Weapon { get; private set; } 

		public AttackOrder(GameObject target, UnitController unit)
		{
			Target = target;
			PathSeeker = unit.PathSeeker;
			Unit = unit;
            Weapon = Unit.Weapons;
        }

        private float repathRate = 0.5f;
        private float lastRepath = -9999;
        
		private bool isStarted = false;
		private bool isComplete = false;
		private bool isCanceled = false;

        private void OnPathFound(Path p) {
            if (isCanceled) return;
            
            p.Claim (follower);
            if (!p.error) {
                if (!follower.IsDone()) follower.Stop();
                follower.MoveAlongPath(p);
            } else {
                follower.Stop();
            }
		}

		public bool Execute()
		{
            if (!isStarted) {
                Weapon.SetTarget(Target);
                isStarted = true;
            }
            
            if (!Weapon.IsTargetVisible()) {
                if (Time.time - lastRepath > repathRate && PathSeeker.IsDone()) {
                    lastRepath = Time.time + Random.value * repathRate * 0.5f;
                    PathSeeker.StartPath(Unit.gameObject.transform.position, Target.transform.position, OnPathFound);
                } 
            } else {
                follower.Stop();
            }
            
            return false;  
		}

		public void Cancel ()
		{
            Weapon.ClearTarget();
            follower.Stop();
			isCanceled = true;
		}
	}
}
