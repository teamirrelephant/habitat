using UnityEngine;
using System.Collections;

using Pathfinding;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Parallel;

namespace Habitat.Game.AI.Unit.Order
{
	public class MoveOrder : IOrder
	{
		public Vector3 Destination { get; private set; }

		public Seeker PathSeeker { get; private set; }

		public UnitController Unit { get; private set; }

        private float repathRate = 0.5f;
        private float lastRepath = -9999;

        private WaypointFollower follower; 
        
		public MoveOrder(Vector3 destination, UnitController unit)
		{
			Destination = destination;
			PathSeeker = unit.PathSeeker;
			follower = unit.Mover;
			Unit = unit;
		}

		private bool isCanceled = false;
		private bool isStarted = false;

		private void OnPathFound(Path p) {
            if (isCanceled) return;
            
            p.Claim (follower);
            if (!p.error) {
                if (!follower.IsDone()) follower.Stop();
                follower.MoveAlongPath(p);
            } else {
                follower.Stop();
            }
		}

		public bool Execute()
		{
            if (!isStarted || !follower.IsDone()) {
				isStarted = true;
                if (Time.time - lastRepath > repathRate && PathSeeker.IsDone()) {
                    lastRepath = Time.time + Random.value * repathRate * 0.5f;
                    PathSeeker.StartPath(Unit.gameObject.transform.position, Destination, OnPathFound);
                }       
                return false;
            }

			return true;
		}

		public void Cancel ()
		{
			follower.Stop();
			isCanceled = true;
		}
	}
}
