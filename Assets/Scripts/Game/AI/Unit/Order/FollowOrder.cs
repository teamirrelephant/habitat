using UnityEngine;
using System.Collections;

using Pathfinding;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Parallel;

namespace Habitat.Game.AI.Unit.Order
{
	public class FollowOrder : IOrder
	{
		public GameObject Target { get; private set; }

		public Seeker PathSeeker { get; private set; }

		public UnitController Unit { get; private set; }

        private float repathRate = 0.5f;
        private float lastRepath = -9999;
		private float followDistanceSquared = 5f;
        private WaypointFollower follower; 
        
		public FollowOrder(GameObject target, UnitController unit)
		{
			Target = target;
			PathSeeker = unit.PathSeeker;
			follower = unit.Mover;
			Unit = unit;
		}

		private bool isCanceled = false;

		private void OnPathFound(Path p) {
            if (isCanceled) return;
            
            p.Claim (follower);
            if (!p.error) {
                if (!follower.IsDone()) follower.Stop();
                follower.MoveAlongPath(p);
            } else {
                follower.Stop();
            }
		}

		public bool Execute()
		{
            if ((Unit.gameObject.transform.position - Target.transform.position).sqrMagnitude > followDistanceSquared) {
                if (Time.time - lastRepath > repathRate && PathSeeker.IsDone()) {
                    lastRepath = Time.time + Random.value * repathRate * 0.5f;
                    PathSeeker.StartPath(Unit.gameObject.transform.position, Target.transform.position, OnPathFound);
                } 
            } else {
                follower.Stop();
            }
            
            return false;  
		}

		public void Cancel ()
		{
			isCanceled = true;
		}
	}
}
