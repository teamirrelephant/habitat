using UnityEngine;

namespace Habitat.Game.AI.Unit.Order
{
	public interface IOrder
	{
		bool Execute();
		void Cancel();
	}
}
