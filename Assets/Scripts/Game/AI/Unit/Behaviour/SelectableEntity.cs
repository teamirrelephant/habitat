using UnityEngine;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Game.Helper;
using Habitat.Game.Player.Camera;

namespace Habitat.Game.AI.Unit.Behaviour {
	
	public class SelectableEntity : MonoBehaviour {

		public EntityController Entity { get; private set; }

		public bool isDragboxSelectable = true;

		private Renderer modelRenderer;

		private Shader defaultShader;

		public void Select() {
			modelRenderer.material.shader = Shader.Find("Particles/Additive");
		}

		public void Deselect() {
			modelRenderer.material.color = Entity.Owner.Color;
			modelRenderer.material.shader = defaultShader;
		}

		void Start() {
			Entity = GetComponent<EntityController>();
			modelRenderer = Entity.RenderModel.GetComponent<Renderer>();
			defaultShader = modelRenderer.material.shader;
		}

		void OnSelectionBox() {
			if (!isDragboxSelectable)
				return;

			if (Entity.Owner != GameController.Instance.CurrentPlayer)
				return;

			if (modelRenderer.isVisible) {
				var unitPositionInScreenSpace = Camera.main.WorldToScreenPoint (Entity.transform.position);
				unitPositionInScreenSpace.y = WorldSpaceHelper.WorldToScreenSpaceY (unitPositionInScreenSpace.y);
				
				if (EntitySelector.Instance.BoxSelectionRectangle.Contains (unitPositionInScreenSpace)) {
					EntitySelector.Instance.NotifyBoxSelection (this);
				}
			}
		}


	}
}