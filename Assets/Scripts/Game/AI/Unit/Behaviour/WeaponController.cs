using UnityEngine;
using System.Collections;

using Habitat.Game.AI.Unit.Controller;
using Habitat.Game.Helper;
using Habitat.Game.Player;
using Habitat.DynamicTerrain;
using Habitat.DynamicTerrain.Deformation;

namespace Habitat.Game.AI.Unit
{
    /// <summary>
    /// Shoots the target with high-velocity projectiles simulated with raycasting.
    /// </summary>
	[RequireComponent(typeof(TargetWatcher))]
    public class WeaponController : MonoBehaviour
    {        
        public float shotCooldown = 3f; 
        
		public float maxRange = 100f;

        public GameObject gunTip;
        
		private TargetWatcher watcher;

		private LineRenderer gunBeam;

		private EntityController primaryTarget;

		private WaitForSeconds shotDelay;

		private WaitForSeconds beamDelay;

		private float beamAnimation = 0.05f;

        #region [ Private fields ]

        /// <summary>
        /// Target to lock on to.
        /// </summary>
        private GameObject targetObject;
        
        #endregion
      
        #region [ Private methods ]
        
		/// <summary>
		/// Raycasts in order to find the appropariate target.
		/// </summary>
		/// <returns>The target if it is reachable.</returns>
		private EntityController RaycastToTarget() {
			var targetEntity = RaycastingHelper.RetrieveRoot(RaycastingHelper.QuickRaycast(gunTip.transform.position, transform.forward, maxRange));
			if (!targetEntity)
				return null;

			var entity = targetEntity.GetComponent<EntityController>();

			if (entity.Owner.GetAttitudeTowards(Owner).IsHostile())
				return entity;
			else
				return null;
		}

		/// <summary>
		/// Applies the hit effect.
		/// </summary>
		/// <param name="entity">Entity.</param>
		void ApplyHitEffect(EntityController entity)
		{
			//TODO: You know what to do.
			entity.Owner = PlayerInfo.Aliens;
		}

		private TerrainDeformation td = new CraterDeformation(1, 1, 0f, 0.0003f).Generate();
		/// <summary>
		/// Performs the shot using coroutine delays.
		/// </summary>
        protected virtual bool Shoot() {
			var target = RaycastToTarget();

			if (target != null) {
				gunBeam.SetPosition(0, gunBeam.transform.position);

				var shotInacuracy = new Vector3 (Random.Range(0f, 0.1f), Random.Range(0f, 0.1f), Random.Range(0f, 0.1f)); 
				var hit = RaycastingHelper.QuickRaycast(gunTip.transform.position, transform.forward + shotInacuracy, maxRange);
				var actualTarget = RaycastingHelper.RetrieveRoot(hit);
				if (actualTarget)
				{
					ApplyHitEffect(actualTarget.GetComponent<EntityController>());
					gunBeam.SetPosition(1, hit.point);
				} else {
					if (hit.point != Vector3.zero) {
						gunBeam.SetPosition(1, hit.point);
						TerrainController.Terrain.ApplyDeformation(td, (int)(hit.point.x * 4), (int)(hit.point.z * 4));
					} else {
						gunBeam.SetPosition(1, gunTip.transform.position + (transform.forward + shotInacuracy) * maxRange);
					}
				}

				gunBeam.enabled = true;
				return true;
			}

			return false;
        }
        
		/// <summary>
		/// Shoots or delays the shot until next frame.
		/// </summary>
		/// <returns>The when ready.</returns>
        private IEnumerator ShootWhenReady() {
            while (true) {
                if (targetObject != null && watcher.LookingAtTarget()) {
                    if (Shoot()) {
						yield return beamDelay;
						gunBeam.enabled = false;
						yield return shotDelay;
					}
                }
				
                yield return null;
            }
        }
        
        #endregion
        
        #region [ GameObject interfacing ]
        
        void Start() {
			watcher = GetComponent<TargetWatcher>();
            StartCoroutine(ShootWhenReady());
			gunBeam = transform.GetChild(0).GetComponent<LineRenderer>();
			shotDelay = new WaitForSeconds (shotCooldown - beamAnimation);
			beamDelay = new WaitForSeconds (beamAnimation);
		}
        
        void Update()
        {
            watcher.RotateTowardsTarget();            
        }

        #endregion

        #region [ Exported functions ]
        
        /// <summary>
        /// Sets the current target for shooting.
        /// </summary>
        /// <param name="target"></param>
        public void SetTarget(GameObject target)
        {
            targetObject = target;
			primaryTarget = target.GetComponent<EntityController>();
            watcher.SetTarget(target);
        }
        
        public GameObject Target {
            get {
                return targetObject;
            }
        }
        
        public bool IsTargetVisible() {
            return RaycastToTarget() == primaryTarget;
        }
        
        public bool IsTargetReachable() {
            return IsReachable(primaryTarget);
        }
        
        public bool IsReachable(Collider c) {
            
        }

        /// <summary>
        /// Clears the current watcher target, rotating it to default position.
        /// </summary>
        public void ClearTarget()
        {
            targetObject = null;
            watcher.ClearTarget();
        }
        
        #endregion
    }

}