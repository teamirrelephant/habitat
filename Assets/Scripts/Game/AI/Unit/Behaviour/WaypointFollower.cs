using Pathfinding;
using UnityEngine;

namespace Habitat.Game.AI
{
    /// <summary>
    /// Follows the provided path. Requires rigidbody.
    /// </summary>
    public class WaypointFollower : MonoBehaviour
    {
        #region [ Imported variables ]

        /// <summary>
        /// Absolute maximum speed of an object.
        /// </summary>
        public float maxSpeed = 5f;

        /// <summary>
        /// Acceleration towards the target
        /// </summary>
        public float acceleration = 0.1f;

        /// <summary>
        /// Deceleraton factor
        /// </summary>
        public float deceleration = 0.2f;

        /// <summary>
        /// Maximum rotation speed.
        /// </summary>
        public float rotation = 90f;

        /// <summary>
        /// Allowed distance error when calculating waypoint reach.
        /// </summary>
        public float nextWaypointDistance = 0.5f;

        /// <summary>
        /// Allowed distance error determining looking-at.
        /// Maximum distance is 2 (Units are looking in opposite directions).
        /// </summary>
        public float lookingDistance = 0.3f;

        #endregion

        #region [ Private fields ]

        /// <summary>
        /// Current path that this gameobject is travelling along.
        /// </summary>
        private Path currentPath;

        /// <summary>
        /// Current speed normalized between 0 and 1f.
        /// </summary>
        private float currentSpeed = 0f;

		/// <summary>
		/// Current waypoint index to go for.
		/// </summary>
		private int currentWaypoint;

		/// <summary>
		/// Unit container's transform.
		/// Rotation of this object must not be changed!
		/// </summary>
		private Transform unitTransform;
        #endregion

        #region [ Private methods ]

        /// <summary>
        /// Accelerates the unit forward.
        /// </summary>
        private void Accelerate()
        {
            currentSpeed = Mathf.Min(currentSpeed + acceleration * Time.deltaTime, 1f);
        }

        /// <summary>
        /// Decelerates the unit.
        /// </summary>
        private void Decelerate()
        {
            currentSpeed = Mathf.Lerp(currentSpeed, 0f, deceleration);
        }

        /// <summary>
        /// Determines whether unit is looking in certain direction with certain margin for error.
        /// </summary>
        /// <param name="direction">Direction to detect.</param>
        /// <returns></returns>
        private bool AlmostLookingAt(Vector3 direction)
        {
			var forward = transform.forward;
			return (forward - direction).magnitude < lookingDistance;
        }

        /// <summary>
        /// Make a movement step.
        /// </summary>
        /// <param name="waypoint"></param>
        private void MoveTowardsWaypoint(Vector3 waypoint)
        {
			var waypointDirection = (waypoint - unitTransform.position).normalized;

            if (AlmostLookingAt(waypointDirection))
            {
                Accelerate();
            }
            else
            {
                Decelerate();
            }

            RotateTowards(waypointDirection);
        }

        /// <summary>
        /// Rotates the unit towards the certain direction.
        /// </summary>
        /// <param name="waypointDirection">Direction to rotate towards.</param>
        private void RotateTowards(Vector3 waypointDirection)
        {
			var ray = new Ray(unitTransform.position + Vector3.up * GameplayConstants.WorldHeight, -Vector3.up);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, GameplayConstants.WorldHeight + GameplayConstants.RaycastingDelta, GameplayConstants.TerrainLayerMask)) {
				var step = rotation * Time.deltaTime;
				transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(waypointDirection, hit.normal), step);
			}
        }

        /// <summary>
        /// Checks if unit has reached the given waypoint.
        /// </summary>
        /// <param name="waypoint"></param>
        /// <returns></returns>
        private bool IsWaypointReached(Vector3 waypoint)
        {
            return (unitTransform.position - waypoint).magnitude < nextWaypointDistance;
        }


        /// <summary>
        /// Moves the gameObejct forward
        /// </summary>
        private void MoveForward()
        {
            if (currentSpeed == 0) return;
			unitTransform.Translate(transform.forward * Time.deltaTime * currentSpeed * maxSpeed);
        }

        #endregion

        #region [ GameObject interfacing ]

        void Update()
        {
            MoveForward();
			if (IsDone()) { 
				Decelerate(); return;
			}
            if (currentWaypoint >= currentPath.vectorPath.Count) { Stop(); return; }

            if (IsWaypointReached(currentPath.vectorPath[currentWaypoint]))
            {
                currentWaypoint++;
            }
            else
            {
                MoveTowardsWaypoint(currentPath.vectorPath[currentWaypoint]);
            }
        }

        void Awake()
        {
			unitTransform = transform.parent;
        }

        #endregion

        #region [ Exported functions ]
        /// <summary>
        /// Sets the path for the unit to move along.
        /// </summary>
        /// <param name="p">Path to move along.</param>
        public void MoveAlongPath(Path p)
        {
            currentPath = p;
            currentWaypoint = 0;
        }

        /// <summary>
        /// Stops the unit movement
        /// </summary>
        public void Stop()
        {
			if (currentPath == null)
				return;

            currentPath.Release(this);
            currentPath = null;
        }
        
        public bool IsDone() {
            return currentPath == null;
        }
        #endregion
    }

}