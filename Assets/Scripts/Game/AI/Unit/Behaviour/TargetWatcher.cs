using UnityEngine;

namespace Habitat.Game.AI.Unit
{
    /// <summary>
    /// Looks at the given target.
    /// </summary>
    public class TargetWatcher : MonoBehaviour
    {
        #region [ Private fields ]
        
        /// <summary>
        /// Maximum rotation speed.
        /// </summary>
        public float rotation = 0.1f;

        /// <summary>
        /// Allowed distance error determining looking-at.
        /// Maximum distance is 2 (Units are looking in opposite directions).
        /// </summary>
        private float lookingDistance = 0.1f;

        /// <summary>
        /// Target to lock on to.
        /// </summary>
        private Transform targetObject;
        
        #endregion
      
        #region [ Private methods ]
        
        /// <summary>
        /// Shows current required rotation.
        /// </summary>
        private Vector3 TargetRotation
        {
            get {
                if (targetObject == null) {
                    return transform.forward;
                }
                return (targetObject.position - transform.position).normalized;
            }
        }
        
        /// <summary>
        /// Rotates the object towards the certain direction.
        /// </summary>
        public void RotateTowardsTarget()
        {
            var step = rotation * Time.deltaTime;
            var newDir = Vector3.RotateTowards(transform.forward, TargetRotation, step, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);
        }

        #endregion

        #region [ Exported functions ]
        
        /// <summary>
        /// Checks if the target watcher is over the target's transform object.
        /// </summary>
        /// <returns></returns>
        public bool LookingAtTarget()
        {
            return (transform.forward - TargetRotation).magnitude < lookingDistance;
        }

        /// <summary>
        /// Sets the current target for lock on.
        /// </summary>
        /// <param name="target"></param>
        public void SetTarget(GameObject target)
        {
            targetObject = target.transform;
        }

        /// <summary>
        /// Clears the current watcher target, rotating it to default position.
        /// </summary>
        public void ClearTarget()
        {
            targetObject = null;
        }
        
        #endregion
    }

}