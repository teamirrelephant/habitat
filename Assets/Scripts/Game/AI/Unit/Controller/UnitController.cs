﻿using UnityEngine;

using Habitat.AI.Unit;
using Habitat.Game.AI.Unit.Order;
using Habitat.Game.Helper;

namespace Habitat.Game.AI.Unit.Controller {

	public class UnitController : EntityController {

		protected OrderQueue orderQueue;

        public Seeker PathSeeker {
            get; private set;
        }

		private GameObject renderModel;
		public override GameObject RenderModel {
			get {
				return renderModel ?? (renderModel = transform.GetChild(0).GetChild(0).gameObject);
			}
		}

		private WeaponController weapons;
		public WeaponController Weapons {
			get {
				return weapons ?? (weapons = transform.GetChild(0).GetChild(1).GetComponent<WeaponController>());
			}
		}

		private WaypointFollower mover;
		public WaypointFollower Mover {
			get {
				return mover ?? (mover = transform.GetChild(0).GetComponent<WaypointFollower>());
			}
		}

		void Start () {
			orderQueue = new OrderQueue();
			PathSeeker = GetComponent<Seeker>();
			if (Owner == null) {
				Owner = GameController.Instance.CurrentPlayer;
			}
		}

		void Update () {
			orderQueue.ExecuteCurrentOrder();
		}

		public virtual void GiveDefaultTerrainOrder(Vector3 destination, bool queue) {
            if (queue)
                orderQueue.AddToQueue(new MoveOrder(destination, this));
            else
                orderQueue.OvewriteQueueWithOrder (new MoveOrder(destination, this));
		}
        
		public virtual void GiveDefaultEntityOrder(GameObject target, bool queue) {
            IOrder order;
            if (target.GetComponent<EntityController> ().Owner.GetAttitudeTowards (Owner).IsHostile ())
				order = new AttackOrder(target, this);
            else 
                order = new FollowOrder(target, this);
            
            if (queue) 
                orderQueue.AddToQueue(order);
            else
                orderQueue.OvewriteQueueWithOrder(order);
		}
	}
}
