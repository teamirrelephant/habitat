using UnityEngine;
using Habitat.Game.Player;

namespace Habitat.Game.AI.Unit.Controller {
	
	public class EntityController : MonoBehaviour {

		private PlayerInfo owner;
		public PlayerInfo Owner {
			get {
				return owner;
			}
			set {
				owner = value;
				RenderModel.GetComponent<Renderer>().material.color = value.Color;
			}
		} 

		public virtual GameObject RenderModel {
			get {
				return transform.GetChild(0).gameObject;
			}
		}

	}
}
