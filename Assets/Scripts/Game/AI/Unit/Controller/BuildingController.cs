using UnityEngine;
using Habitat.Game.Player;

namespace Habitat.Game.AI.Unit.Controller {
	
	public class BuildingController : EntityController {
		void Start() {
			AstarPath.active.UpdateGraphs (new Bounds (transform.position, Vector3.one * 10f));
		}		
	}
}
