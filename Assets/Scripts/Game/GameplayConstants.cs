namespace Habitat.Game
{
	/// <summary>
	/// Contains the gameplay constants.
	/// </summary>
	public static class GameplayConstants
	{
		#region [ Raycasting ]
		/// <summary>
		/// The height of the world. Used in terrain raycasting and Superterrain generation.
		/// </summary>
		public const float WorldHeight = 512f;

		/// <summary>
		/// Maximum unit height over the terrain. Used for raycasting.
		/// </summary>
		public const float RaycastingDelta = 100f;

		#endregion

		#region [ Layers ]
		/// <summary>
		/// Number of the "Terrain" layer
		/// </summary>
		public const int TerrainLayer = 8;

		/// <summary>
		/// Calculated mask for raycasting against the terrain.
		/// </summary>
		public const int TerrainLayerMask = 1 << TerrainLayer;

		/// <summary>
		/// Number of the "Units" layer
		/// </summary>
		public const int UnitsLayer = 9;
		
		/// <summary>
		/// Calculated mask for raycasting against the units.
		/// </summary>
		public const int UnitsLayerMask = 1 << UnitsLayer;

		/// <summary>
		/// Number of the "Buildings" layer
		/// </summary>
		public const int BuildingsLayer = 10;
		
		/// <summary>
		/// Calculated mask for raycasting against the buildings.
		/// </summary>
		public const int BuildingsLayerMask = 1 << BuildingsLayer;

		#endregion

		#region [ Superterrain ]
		/// <summary>
		/// Superterrain part side size.
		/// </summary>
		public const int SubterrainSize = 64;

		/// <summary>
		/// Maximum superterrain height.
		/// </summary>
		public const int SuperTerrainHeightHeight = 512;

		/// <summary>
		/// Heightmap resolution for the SuperTerrain.
		/// </summary>
		public const int SuperTerrainHeightmapResolution = 256;

		/// <summary>
		/// Pixel error for the SuperTerrain.
		/// </summary>
		public const int SuperTerrainPixelError = 1;

		#endregion
	}
}

