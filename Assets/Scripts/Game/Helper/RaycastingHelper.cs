using UnityEngine;

namespace Habitat.Game.Helper {

	public static class RaycastingHelper {
		/// <summary>
		/// Gets the object which is pointed at by the mouse pointer.
		/// </summary>
		/// <returns>The pointing target.</returns>
		/// <param name="layer">Layer.</param>
		public static RaycastHit GetPointingTarget(int layer) {
			RaycastHit hit;
			Physics.Raycast(UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition), out hit, GameplayConstants.WorldHeight, layer);
			return hit;
		}

		/// <summary>
		/// Performs the basic the raycast.
		/// </summary>
		/// <returns>The raycast.</returns>
		/// <param name="origin">Origin for the raycast</param>
		/// <param name="direction">Direction of the raycast</param>
		/// <param name="distance">Distance to raycast</param>
		public static RaycastHit QuickRaycast(Vector3 origin, Vector3 direction, float distance) {
			RaycastHit hit;
			var ray = new Ray(origin, direction);
			Physics.Raycast (ray, out hit, distance);
			return hit;
		}

		/// <summary>
		/// Retrieves the container of a raycast hit entity (building/unit).
		/// Requires the object with collider to have a ContainerReference attached.
		/// </summary>
		/// <returns>The root.</returns>
		/// <param name="hit">Hit.</param>
		public static GameObject RetrieveRoot(RaycastHit hit) {
			if (hit.collider) {
				var containerRef = hit.collider.GetComponent<ContainerReference>();
				if (containerRef) {
					return containerRef.rootContainer;
				}
			}
			return null;
		}


	}
}