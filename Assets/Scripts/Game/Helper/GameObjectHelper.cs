using UnityEngine;

namespace Habitat.Game.Helper {
	public static class GameObjectHelper {

		/// <summary>
		/// Sets the layer recursively on the target object and all the child objects.
		/// </summary>
		/// <param name="obj">Object.</param>
		/// <param name="layer">Layer.</param>
		/// <param name="overrideCurrent">If set to <c>true</c> override current.</param>
		public static void SetLayerRecursive(GameObject obj, int layer, bool overrideCurrent = false) {
			foreach (Transform trans in obj.GetComponentsInChildren<Transform>(true)) {
				if (overrideCurrent || trans.gameObject.layer == 0)
					trans.gameObject.layer = layer;
			}
		}
	}
}