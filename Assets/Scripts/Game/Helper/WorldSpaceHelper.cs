using UnityEngine;

namespace Habitat.Game.Helper {

	public static class WorldSpaceHelper {
	
		/// <summary>
		/// Converts the Y coordinate to screen space from the world space.
		/// </summary>
		/// <returns>The to screen space y.</returns>
		/// <param name="y">The y coordinate.</param>
		public static float WorldToScreenSpaceY(float y) {
			return Screen.height - y;
		}

	}
}