using Habitat.DataStructures;

namespace Habitat.Game.Helper {
    public static class DiplomacyHelper {
        public static bool IsHostile(this DiplomacyState s) {
            return s == DiplomacyState.NeutralHostile || s == DiplomacyState.Hostile;        
        }
        
        public static bool IsWorseThan(this DiplomacyState s, DiplomacyState other) {
             return ((int)s - (int)other) > 0;
        }
    }

}